<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Kết bạn bốn phương - Trang Đăng ký</title>
</head>

<body style="font-family:arial,verdana,serif;">
<form method="post" name="form1" id="form1" action="registration.php">
	<table align="center" width="460" border="0" cellpadding="3" cellspacing="15" bgcolor="#9966CC" style="color:#FFF;">
		<tr bgcolor="#bb0675">
			<td height="60" colspan="2" align="center" bgcolor="#bb0675"><strong style="font-size: 36px; color: #FFF;">Kết Bạn Bốn Phương</strong></td>
		</tr>
		
		<tr>
			<td colspan="2">Trở về <a href="index.php" title="Trở về Trang thông tin">Trang thông tin</a></td>
		</tr>
		<tr>
			<td width="122" align="right"><strong>Họ và tên:</strong></td>
			<td><input name="textName" type="text" required id="textName" placeholder="Nhập Họ tên" style="width: 300px;"></td>
		</tr>
		
		<tr>
			<td align="right"><strong>Giới tính:</strong></td>
			<td width="305">
				<label><input type="radio" name="rgroupSex" value="0" id="rgroupSex_Male" checked>Nam</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<label><input type="radio" name="rgroupSex" value="1" id="rgroupSex_Female">Nữ</label>
			</td>
		</tr>
		
		<tr>
			<td align="right"><strong>Năm sinh:</strong></td>
			<td>
				<select name="selectYear" id="selectYear" title="Lựa chọn năm sinh" style="width:80px;">
					<option value="1990">1990</option>
					<option value="1991">1991</option>
					<option value="1992">1992</option>
					<option value="1993">1993</option>
					<option value="1994">1994</option>
					<option value="1995">1995</option>
					<option value="1996">1996</option>
					<option value="1997">1997</option>
					<option value="1998">1998</option>
					<option value="1999">1999</option>
					<option value="2000">2000</option>
				</select>
			</td>
		</tr>
		
		<tr>
			<td align="right" valign="top"><strong>Sở thích:</strong></td>
			<td>
				<table width="299">
					<tr>
						<td width="150"><label>
								<input type="checkbox" name="cgroup[]" value="Mua sắm"/>
								Mua sắm</label></td>
						<td width="137"><label>
								<input type="checkbox" name="cgroup[]" value="Xem tivi"/>
								Xem tivi</label></td>
					</tr>
					<tr>
						<td><label>
								<input type="checkbox" name="cgroup[]" value="Đọc báo"/>
								Đọc báo</label></td>
						<td><label>
								<input type="checkbox" name="cgroup[]" value="Thể thao"/>
								Thể thao</label></td>
					</tr>
					<tr>
						<td><label>
								<input type="checkbox" name="cgroup[]" value="Du lịch"/>
								Du lịch</label></td>
						<td><label>
								<input type="checkbox" name="cgroup[]" value="Nghiên cứu"/>
						Nghiên cứu</label></td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr>
			<td height="100" align="right" valign="top"><strong>Lời muốn nói:</strong></td>
			<td valign="top"><textarea name="textMess" id="textMess" style="width: 300px; height:90px;" placeholder="Nhập lời muốn nói" required></textarea></td>
		</tr>
		
		<tr>
			<td align="right">&nbsp;</td>
			<td align="left"><input type="submit" name="btnSubmit" id="btnSubmit" value="Đăng ký">
				&nbsp; &nbsp;
				<input type="reset" name="btnReset" id="btnReset" value="Nhập lại"></td>
		</tr>
	</table>

</form>
</body>
</html>
